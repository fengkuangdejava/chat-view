const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  publicPath: "/", // 路由访问根路径
  outputDir: "dist/", //输出文件路径
  productionSourceMap: false,
  lintOnSave: false,
  devServer: {
    https: false,
    hot: true,
    proxy: {

      "/dev": {
        // target: "http://localhost:8080", //本地环境
        // target: 'http://servermake.preview.yinghucloud.com',//预发布环境
        // target: "http://servermake.beta.yinghucloud.com", //测试环境
        target: "http://servermake.alpha.yinghucloud.com", //开发环境
        secure: false, //  true: 接受对方是https的接口
        changeOrigin: true,
        pathRewrite: {
          "/dev": "",
        },
      },
    },
  },
})

import Vue from "vue";
import Vuex from "vuex";
import user from "./modules/user";
import contacts from "./modules/contacts";
import messageList from "./modules/messageList";
Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    user,
    contacts,
    messageList,
  },
});

export default {
  namespaced: true,
  state: {
    contactsData: [
      {
        id: 1,
        displayName: "ChatGPT机器人1",
        avatar: "https://p.qqan.com/up/2020-2/2020022821001845128.jpg",
        index: "X",
        unread: 0,
        lastSendTime: 1566047865417,
        lastContent: "",
      },
      {
        id: 2,
        displayName: "ChatGPT机器人2",
        avatar: "https://p.qqan.com/up/2020-2/2020022821001845128.jpg",
        index: "Y",
        unread: 0,
        lastSendTime: 1566047865417,
        lastContent: "",
      },
    ],
  },
  getters: {},
  mutations: {},
  actions: {},
};

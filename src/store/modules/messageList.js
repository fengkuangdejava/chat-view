import { setMsgCookie, getMsgCookie } from "@/utils/auth";
// 生成消息唯一索引
const generateRandId = () => {
  return Math.random().toString(36).substr(-8);
};
const userData = {
  id: 1000,
  avatar: "https://p.qqan.com/up/2018-4/15244505348390471.jpg",
  displayName: "野火",
};

export default {
  namespaced: true,
  state: {
    messageList: {
      // key : 好友id
      // value : 历史消息
      //   1: [
      //   {
      //     id: generateRandId(), //消息唯一索引
      //     status: "succeed",
      //     type: "text",
      //     sendTime: 1566047865417,
      //     content: "问你件事",
      //     toContactId: 1,
      //     fromUser: userData,
      //   },
      // {
      //   id: generateRandId(),
      //   status: "succeed",
      //   type: "text",
      //   sendTime: 1566047865417,
      //   content: "欢迎体验AI聊天,made by 胖虎,晓月 Email:fengkuangdejava@outlook.com,luxiaoyue@qq.com",
      //   toContactId: 1,
      //   fromUser: {
      //     id: 1,
      //     displayName: "ChatGPT机器人",
      //     avatar: "https://p.qqan.com/up/2020-2/2020022821001845128.jpg",
      //   },
      // },
      //   ],
    },
  },
  getters: {},
  mutations: {
    SETCONTACTMESSAGELIST(state, { id, list }) {
      if (!state.messageList[id]) state.messageList[id] = [];
      state.messageList[id] = list;
    },
    PUSHMSG(state, { id, msgObj }) {
      if (!state.messageList[id]) state.messageList[id] = [];
      state.messageList[id].push(msgObj);
    },
  },
  actions: {
    initMsgList({ commit }, { id, initMsgList }) {
      let list = [];
      // 初始化本地聊天记录
      let localData = getMsgCookie() ? JSON.parse(getMsgCookie()) : {};
      if (initMsgList) {
        localData[id] = list = initMsgList;
        setMsgCookie(JSON.stringify(localData));
      } else {
        if (localData[id]) {
          list = localData[id];
        } else {
          localData[id] = [];
          setMsgCookie(JSON.stringify(localData));
        }
      }
      commit("SETCONTACTMESSAGELIST", { id, list });
    },
    pushMsg({ commit }, { id, msgObj }) {
      let localData = getMsgCookie() ? JSON.parse(getMsgCookie()) : {};
      if (localData[id]) {
        localData[id].push(msgObj);
      } else {
        localData[id] = [];
        localData[id].push(msgObj);
      }
      setMsgCookie(JSON.stringify(localData));
      commit("PUSHMSG", { id, msgObj });
    },
    removeMsgById({ commit }, { id, msgId }) {
      let localData = getMsgCookie() ? JSON.parse(getMsgCookie()) : {};
      if (localData[id]) {
        let index = localData[id].findIndex((i) => i.id == msgId);
        index > -1 && localData[id].splice(index, 1);
      } else {
        localData[id] = [];
      }
      let list = localData[id];
      setMsgCookie(JSON.stringify(localData));
      commit("SETCONTACTMESSAGELIST", { id, list });
    },
  },
};

import Cookies from "js-cookie";

let token = "chatAccountId";

export const getCookie = (options) => {
  return Cookies.get(token, options);
};

export const setCookie = (value, options) => {
  return Cookies.set(token, value, options);
};

export const removeCookie = (options) => {
  return Cookies.remove(token, options);
};
const messageCookieKey = "localMsg"; //本地历史消息

export const getMsgCookie = (options) => {
  return Cookies.get(messageCookieKey, options);
};

export const setMsgCookie = (value, options) => {
  return Cookies.set(messageCookieKey, value, options);
};

export const removeMsgCookie = (options) => {
  return Cookies.remove(messageCookieKey, options);
};
